/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pa;

import annotation.Fonction;
import java.util.ArrayList;
import java.util.HashMap;
import modelview.ModelView;

/**
 *
 * @author dina
 */
public class Emp {
    int id;
    String names;
    double age;

    public Emp(String names, double age) {
        this.names = names;
        this.age = age;
    }

    public Emp() {
    }
    
    public double getAge() {
        return age;
    }

    public void setAge(double age) {
        this.age = age;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String name) {
        this.names = name;
    }
     @Fonction(url = "/liste-Emp")
    public ModelView getListEmp() throws Exception {
        ModelView returner = new ModelView();
        ArrayList<Emp> out =new ArrayList();
        out.add(new Emp("Jean",15));
        out.add(new Emp("rabe",14));
        out.add(new Emp("Rakoto",17));
        out.add(new Emp("Randria",11));
        out.add(new Emp("Rajo",29));
        out.add(new Emp("Johary",28));
        out.add(new Emp("Harry",12));
        returner.setPage("page.jsp");
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("liste", out);
        data.put("idSession", 12);
        returner.setData(data);
        return returner;
    }
     @Fonction(url = "/save-Emp")
    public ModelView saveEmp() throws Exception {
        ModelView returner = new ModelView();
          returner.setPage("success.jsp");
       Emp vao=new Emp(this.names, this.age);
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("message", "Save Emp");
        returner.setData(data);
        return returner;
    }
      @Fonction(url = "/form-Emp")
    public ModelView toForm() throws Exception {
        ModelView returner = new ModelView();
       Emp vao=new Emp(this.names, this.age);
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("message", "Save Emp");
          returner.setPage("formulaire.jsp");
        returner.setData(data);
        return returner;
    }  @Fonction(url = "/page-Emp")
    public ModelView toPage() throws Exception {
        ModelView returner = new ModelView();
          returner.setPage("page.jsp");
           HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("message", "Save Emp");
        returner.setData(data);
        return returner;
    }
}
